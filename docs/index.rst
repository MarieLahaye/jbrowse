.. jbrowse documentation master file, created by
   sphinx-quickstart on Mon Sep 21 10:32:15 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive, pour ajouter un sommaire afin de grouper tous les fichiers composants
   la documentation

===================
Jbrowse de la vigne
===================

.. toctree::
   :maxdepth: 2

   introduction
   informations_generales
   gestion_des_tracks
   integration_donnees
   autres_scripts