==============
Autres scripts
==============

Scripts python
--------------

``add_name_bed.py``
~~~~~~~~~~~~~~~~~~~
Pour afficher une tooltip, il faut obligatoirement que le feature ait un *name*, sinon l'option *Onclick* va planter et il ne sera pas possible de cliquer sur le feature et d'afficher ses informations. Lors de la génération de fichiers BED (comme lors de la récupération des *NGaps*), aucun nom n'est donné. Le script ``add_name_bed.py`` permet donc de donner à chaque feature un nom.

	-i, --input bedFileOri  	Fichier BED d'origine sans *name* pour les features
	-o, --output bedFile  		Fichier BED dont les features portent des noms

**Exemple de commande**

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/add_name_bed.py -i Riesling_Ngaps_without_name.bed -o Riesling_Ngaps.bed

``add_note_gff.py``
~~~~~~~~~~~~~~~~~~~
Pour afficher une description des tRNA et des ncRNA sur le track "Gene annotations", il faut ajouter une note au niveau de la dernière colonne des lignes correspondant à ces types de transcrits (il n'est pas nécessaire de le faire pour les rRNA car ceux-ci en possèdent déjà une en général). Pour cela, le script ``RNA_note_gff.py`` peut lire le fichier donné en entrée et renvoie en sortie un autre fichier gff avec les notes ajoutées.

	-i, --input gffFileOri 		Fichier GFF d'origine sans note
	-o, --output gffFile 		Fichier GFF avec des notes en plus pour les tRNA et les ncRNA

**Exemple de commande**
A partir du dossier où se trouvent les fichiers GFF, le script peut être lancé avec la commande suivante:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/add_note_gff.py -i sequences.ori.gff3 -o sequences.gff3

``bed_cds_alt.py``
~~~~~~~~~~~~~~~~~~


``bed_contigs.py``
~~~~~~~~~~~~~~~~~~
Script pour générer un bed à partir d'un fichier comprenant la liste des contigs primaires. Ce fichier doit contenir 4 colonnes: chromosome, nom du contig, taille et gap entre ce contig et le suivant.

	-i, --input contigFileList  Fichier TXT ou TSV - liste des contigs primaires
	-o, --output bedContigs  	Fichier BED des contigs primaires

**Exemple de commande**:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/bed_contigs.py -i list_contigs.txt -o contigs.bed

``bed_haplotigs.py``
~~~~~~~~~~~~~~~~~~~~
Après l'alignement des haplotigs contre la séquence de référence, il permet de générer le bed des haplotigs en partant du fichier txt de sortie du script ``fc_coords.py``.

	-i, --input txtFile  		Fichier des haplotigs avec: positions sur la séquence de référence et % d'identité
	-o, --output bedHaplotigs  	Fichier BED des contigs primaires

**Exemple de commande**:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/bed_haplotigs.py -i haplotigs_alignments.txt -o haplotigs.bed

``coords_to_bed.py``
~~~~~~~~~~~~~~~~~~~~
A partir du ``.coords`` qui contient les coordonnées des alignements de chaque haplotig, il faut générer un fichier BED afin de pouvoir filtrer les alignements et ne garder que les bons.

	-i, --input coordsFile  	Fichier COORDS contenant les alignements des haplotigs
	-o, --output bedFile 		Fichier BED des coordonnées des alignements

**Exemple de commande**

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/coords_to_bed.py -i RI_haplotigs_alignments.coords.out -o haplotigs_coords.bed

``fasta_to_bed.py``
~~~~~~~~~~~~~~~~~~~
Script permettant de convertir un fichier FASTA en un fichier BED. Il récupère la chromosome, le start et le end, l'id de la séquence.

	-i, --input fastaFile		Fichier FASTA à convertir
	-o, --output bedFile 		Fichier BED contenant les coordonnées des séquences du FASTA

**Exemple de commande**

.. code-block:: bash

	/var/www/html/jbrowse_dev/data/scripts/fasta_to_bed.py -i sequences_cds_ref.fna -o cds_ref.bed

``fc_coords2hp.py``
~~~~~~~~~~~~~~~~~~~
Ce script prend en entrée un fichier COORDS qui contient les coordonnées des alignements locaux des haplotigs sur une séquence de référence par exemple. A partir de ces alignements, il positionne à partir des alignements locaux les haplotigs sur la séquence de référence. En sortie, il renvoie un fichier contenant les coordonnées de chaque haplotig aligné sur la séquence de référence.

Le seul argument à mettre dans la ligne de commande est un argument positionnel, et il correspond au fichier COORDS donné en entrée. L'output du fichier est envoyé directement dans le shell, donc il faut rediriger avec un "**>**" la sortie du script.

**Exemple de commande**

.. code-block:: bash

	/var/www/html/jbrowse_dev/data/scripts/fc_coords2hp.py RI_haplotigs_alignments_filter.coords.out | sort -k6,6 -k8,8n > RI_haplotigs_alignments_filter.hp.txt

``filter_coords.py``
~~~~~~~~~~~~~~~~~~~~
Filtre des alignements locaux des haplotigs. Il prend en entrée un fichier COORDS et filtre les haplotigs phase 0 dont le pourcentage d'identité d'un alignement vaut 100% et la longueur de celui-ci est égale à la longueur de l'haplotig +/- 100 bases. Il filtre en même temps les alignements d'un haplotig sur un mauvais contig primaire, à partir d'un fichier ``intersect.tab`` entre les alignements des haplotigs et les contigs primaires.

	-c, --coords coordsFile		Alignements locaux des haplotigs sur les contigs primaires
	-i, --intersect interFile   Fichier intersect généré avec **bedtools intersect** entre haplotigs/contigs primaires
	-o, --output outputFile 	Output COORDS filtré

**Exemple de commande**

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/filter_coords.py -c RI_haplotigs_alignments.coords.out -i intersect.tab -o RI_haplotigs_alignments_filter.coords.out

``formatting_fna_gff.py``
~~~~~~~~~~~~~~~~~~~~~~~~~
Ce script prend en charge les fichiers FASTA, FNA et GFF. Il permet de filtrer les haplotigs phase 0 en donnant en entrée une liste de phase 0 à garder. Il permet aussi de renommer les gènes en donnant un fichier de correspondance, par exemple gène alt/ref. Et de rajouter des informations dans la colonne **note** du GFF, si cle fichier de correspondance contient des informations dans la 3ème colonne.

	-filter-rename 				Argument positionnel à choix - **filter** ou **rename**
	-i, --input inputFile 		Fichier FASTA, GFF ou FNA
	-g, --geneList geneList 	Fichier contenant les correspondances entre gènes pour renommer ou la liste des phase 0 à garder
	-o, --output outputFile 	Fichier output filtré ou avec gènes renommés - FASTA, GFF, FNA

**Exemple de commande**

.. code-block:: bash

	/var/www/html/jbrowse_dev/data/scripts/formatting_fna_gff.py filter -i ori_files/sequences.gff3 -g genes_phase_0_filtered.txt -o filter_phase_0/sequences.filtered.gff3
	/var/www/html/jbrowse_dev/data/scripts/formatting_fna_gff.py rename -i filter_phase_0/sequences.filtered.fna -g genes_list.tsv -o renamed/sequences.rn.gff3

``gff_to_bed.py``
~~~~~~~~~~~~~~~~~
Convertir un fichier GFF en BED. Le fichier en output contiendra uniquement les coordonnées des gènes, et pas des CSD, mRNA etc... Il permet aussi de récupérer certains gènes en spécifiant avec une option des IDs ou portions d'IDs de gènes.

	-i, --input gffFile 		 Fichier GFF contenant les gènes à mettre au format BED
	-id, --listID [ID1 ID2 ...]  List d'IDs de gènes à récupérer - optionnel
	-o, --output bedFile 		 Fichier BED des gènes

**Exemple de commande**
Ici on souhaitait mettre au format BED tous les gènes situés sur des haplotigs phase 0 donc ceux dont l'ID comprenait la chaine de caractère "phase_0":

.. code-block:: bash

	/var/www/html/jbrowse_dev/data/scripts/gff_to_bed.py -i sequences.gff3 -o genes_phase_0.bed -id phase_


``junctions_positions.py``
~~~~~~~~~~~~~~~~~~~~~~~~~~
Ce script permet d'indiquer les réelles positions des jonctions. L'outils regtools génère à partir de fichiers d'alignement BAM, des fichiers ``junctions.bed``, qui donnent pour chaque jonction, sa position de début et de fin. Or, ces jonctions commencent au début du 1er read et finissent à la fin du 2ème et nous souhaitons avoir les jonctions entre 2 reads, ou 1 read coupé en deux. Ce script permet donc de recalculer les vraies positions des jonctions.

	-i, --input bedFileOri  	Fichier BED d'origine avec les mauvaises positions
	-o, --output bedFile  		Fichier BED de sortie avec les positions recalculées

**Exemple de commande**
Le script peut donc être lancé avec une commande telle que:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/junctions_positions.py -i RI_S1_571.ori.junctions.bed RI_S1_571.junctions.bed


Scripts bash
------------

