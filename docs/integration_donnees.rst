=====================================
Traitement et intégration des données
=====================================
Toutes les données sont ajoutées dans le dossier ``data/``. Pour chaque génome, il est possible de créer un dossier, qu'il faudra renseigner dans le fichier de configuration général ``jbrowse.conf`` (les génomes sont alors accessibles sur le jbrowse):

.. code-block:: bash

	[datasets.Riesling]
	url = ?data=data/Riesling
	name = Riesling

*Note*: L'utilisation de fichiers indexés est recommandée car ils sont plus rapides à parcourir pour le jbrowse. La génération des index sera aussi abordée pour chacun des format de fichiers dans les sections suivantes. Ces fichiers doivent être situés dans le même dossier que les fichiers non indexés afin qu'ils soient reconnus et utilisés par le jbrowse.


Génome de référence (FASTA)
---------------------------
Génération d'un fichier indexé, au format **.fai**, à partir du fasta (**.fa**) contenant la séquence de référence.

.. code-block:: bash

	samtools faidx Riesling_pseudomolecules_ctgP.fa

Création du track:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/initialization_tracks.py -c trackList.json -a Riesling_pseudomolecules.fa


Annotations des gènes (GFF3)
----------------------------
Création d'un lien symbolique vers le fichier d'annotations ``sequences.gff3`` dans le répertoire de travail ``/data2/malahaye/data_jbrowse/Riesling/``

.. code-block:: bash

	ln -s 2020_annotations_RI_REF/outdir.RI_REF/sequences.gff3 RI_annotations.ori.gff3

Tout d'abord, il faut ajouter des notes aux tRNA et rRNA:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/add_note_gff.py -i RI_annotations.ori.gff3 -o RI_annotations.gff3

Avant de pouvoir créer l'index d'un fichier gff3, celui-ci doit être trié, puis compressé. Ensuite seulement, il est possible de lancer la commande **tabix** pour indexer le fichier:

.. code-block:: bash

	gt gff3 -sortlines -tidy RI_annotations.gff3 > RI_annotations.sorted.gff3
	bgzip RI_annotations.sorted.gff3
	tabix -p gff RI_annotations.sorted.gff3.gz

Création du track:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/initialization_tracks.py -c trackList.json -a RI_annotations.sorted.gff3.gz

Pour générer les noms d'index afin de pouvoir rechercher un gène en particulier sur le jbrowse:

.. code-block:: bash

	/var/www/html/jbrowse/bin/generate-names.pl --out .

Ce script sera à relancer dès qu'une modification sera faite dans le fichier gff ou que l'on souhaite modifier les noms de features à indexer.

*Note*: Dans le cas où vous n'utilisez pas la bonne version de Perl, il vous faudra l'exporter à l'aide des commandes:

.. code-block:: bash

	export PATH=/usr/bin/:$PATH
	export PERL5LIB=""


Fichiers d'alignement (BAM)
---------------------------
Les fichiers d'alignement BAM peuvent servir à représenter différents type de tracks mais aussi à générer d'autres types de fichiers comme des BEDs ou des BigWigs. 

Génération des fichiers BAM

.. code-block:: bash

	sbatch -c 24 -w node001 /data2/malahaye/data_jbrowse/Riesling/RNAseq_data/analyse_RNAseq_RI/GREAT_run.sh

Génération des index des fichiers BAM (pensez à modifier le chemin au début du script):

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/indexbam.sh Riesling

Création des tracks:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/initialization_tracks.py -c trackList.json -a BAM_RNAseq/*.bam -m RI_metadata.json

*Note*: Si vous avez généré de nouveaux fichiers BAM et donc de nouveaux fichiers indexés **.bai**, il faudra vider le cache pour éviter toute erreur.


Représentation de la densité des alignements (BigWig)
-----------------------------------------------------
La densité de reads alignés sur chaque portion du génome (RNAseq ici) peut être représentée sous forme d'un graph XY ou sous de variation d'intensité de couleur. Les BigWig sont générés à partir des fichiers WIG qui eux sont générés à partir des fichiers d'alignements BAM.

Génération des BigWig à partir des BAM (après avoir changé le chemin vers le répertoire où ils vont être générés):

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/bamtobigwig.sh Riesling

Création des tracks:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/initialization_tracks.py -c trackList.json -a WIG_RNAseq/*.bw -m RI_metadata.json


Représentation des N gaps (BED)
-------------------------------
Pour récupérer les régions du génome où se trouvent des Ns. L'outil **seqtk** permet, à partir du fichier FASTA de la séquence de référence, de générer un fichier BED contenant toutes les coordonnés des portions de séquences représentées par un ou plusieurs Ns. Après s'être placé dans le dossier contenant le fichier fasta:

.. code-block:: bash

	seqtk cutN -n 1 -p 10000 -g Riesling_pseudomolecules_ctgP.fa > Riesling_Ngaps_without_name.bed
	/var/www/html/jbrowse/data/scripts/add_name_bed.py -i Riesling_Ngaps_without_name.bed -o Riesling_Ngaps.bed

Comme pour le fichier GFF, le BED doit être trié, compressé, puis indexé:

.. code-block:: bash

	sortBed -i Riesling_Ngaps.bed > Riesling_Ngaps.sorted.bed
	bgzip Riesling_Ngaps.sorted.bed
	tabix -p bed Riesling_Ngaps.sorted.bed.gz

Enfin, création des tracks pour le fichier BED des N gaps:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/initialization_tracks.py -c trackList.json -a Riesling_Ngaps.sorted.bed.gz


Sashimi plot (BED)
------------------
Le plugin n'étant pas installé d'office, il faut l'ajouter en suivant les étapes présentées dans la partie :ref:`Ajout des plugins <plugins>`. Ensuite les fichiers BED sont créés à partir des BAMs RNAseq:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/bedsashimi.sh

Création des tracks pour chaque échantillon:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/initialization_tracks.py -c trackList.json -a BED_RNAseq/*.junctions.sorted.bed.gz -m RI_metadata.json


Données RNAseq mergées
----------------------
Dans les dossiers Riesling et Gewurztraminer créer un dossier nommé merged_RNAseq:
	
.. code-block:: bahs

	mkdir merged_RNAseq
	cd merged_RNAseq

Merge des BAMs
~~~~~~~~~~~~~
Tout d'abord, il faut faire le merge des fichier d'alignement BAMs:

.. code-block:: bash

	samtools merge merged.bam /data2/malahaye/data_jbrowse/Riesling/BAM_RNAseq/*.bam
	samtools index merged.bam
	/var/www/html/jbrowse_dev/data/scripts/initialization_tracks.py -c trackList.json  -a merged_RNAseq/merged.bam

Génération du fichier BigWig
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A partir de ces données mergées, il est possible de générer des fichiers BigWigs puis de les intégrer au JBrowse:

.. code-block:: bash

	module load wigToBigWig/1.0
	ln -s /data2/malahaye/data_jbrowse/Riesling/WIG_RNAseq/RI.chrom.sizes
	bam2wig -t merged merged.bam > merged.wig
	wigToBigWig merged.wig RI.chrom.sizes merged.bw
	/var/www/html/jbrowse_dev/data/scripts/initialization_tracks.py -c trackList.json -a merged_RNAseq/merged.bw

Génération du BED des sashimis plot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Et enfin, à partir du BAM mergé on peut générer le BED afin de représenter les sashimis plots

.. code-block:: bash

	module load regtools/0.5.2
	regtools junctions extract -s 2 -t XS merged.bam -o merged.ori.junctions.bed
	/var/www/html/jbrowse_dev/data/scripts/junctions_positions.py -i merged.ori.junctions.bed -o merged.junctions.bed

Tri, compression, indexation du fichier BED

.. code-block:: bash

	sortBed -i merged.junctions.bed > merged.junctions.sorted.bed
	bgzip merged.junctions.sorted.bed
	tabix -p bed merged.junctions.sorted.bed.gz

Et enfin, création du track du BED des données mergées:

.. code-block:: bash

	/var/www/html/jbrowse_dev/data/scripts/initialization_tracks.py -c trackList.json -a merged_RNAseq/merged.junctions.sorted.bed.gz

Contigs primaires (BED)
-----------------------
La liste de contigs primaires et de leur taille est contenue dans le fichier ``NewNames_OperaFormat.txt`` (généré, à partir duquel peut être généré un fichier BED:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/bed_contigs.py -i NewNames_OperaFormat.txt -o contigs.bed

Tri, compression et indexation:

.. code-block:: bash

	sortBed -i contigs.bed > contigs.sorted.bed
	bgzip contigs.sorted.bed
	tabix -p bed contigs.sorted.bed.gz

Création du track des contigs:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/initialization_tracks.py -c trackList.json -a contigs/contigs.sorted.bed.gz


Haplotigs (BED)
---------------
Génération du fichier BED contenant les positions des haplotigs par rapport à la séquence de référence et le pourcentage d'identité des alignements:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/bedhaplotigs.sh

Tri, compression et indexation:

.. code-block:: bash

	sortBed -i haplotigs.bed > haplotigs.sorted.bed
	bgzip haplotigs.sorted.bed
	tabix -p bed haplotigs.sorted.bed.gz

Création du track représentant les haplotigs:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/initialization_tracks.py -c trackList.json -a RI_alignments_haplotigs/haplotigs.sorted.bed.gz


Filtre des fichiers de données de séquences et d'annotations (FASTA, FNA et GFF)
-------------------------------------------------------------------------------
L'alignement des haplotigs sur la séquence de référence permet aussi de filtrer les portions d'haplotigs phase 0 à 100% d'identité. Le script ``bed_haplotigs.py`` génère le fichier ``haplotigs_coords_phase_0.bed`` qui contient les coordonnées sur les haplotigs phase 0 qui ne sont pas filtrées.
A partir de ce fichier, il est possible de filtrer les fichiers de séquence des haplotigs et des gènes alt avec le script ``filterphase0.sh``, qui demande en argument **Riesling** ou **Gewurztraminer**:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/filterphase0.sh Riesling


Genes alt (BED)
---------------
Alignement des cds alt sur la ref, puis différentes étapes de filtration pour enfin générer le bed des gènes alternatifs - alleles.

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/bedcdsalt.sh

Tri, compression et indexation:

.. code-block:: bash

	sortBed -i cds_alt.bed > cds_alt.sorted.bed
	bgzip cds_alt.sorted.bed
	tabix -p bed cds_alt.sorted.bed.gz

Création du track dans le fichier de configuration:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/initialization_tracks.py -c trackList.json -a RI_alignments_cds/cds_alt.sorted.bed.gz


Renommage des gènes alt (FASTA, FNA et GFF)
-------------------------------------------
L'étape précédente aligne les CDS alt contre les CDS ref, le script ``bed_cds_alt.py`` génère le fichier ``genes_list.tsv`` contenant les correspondances (si elles existent), entre les CDS alt et ref.
Avec ces correspondances, on peut utiliser le script ``renamegenes.sh`` pour renommer les gènes alt dans les fichiers FASTA, FNA et GFF, en indiquant comme argument s(il s'agit des données du **Riesling** ou du **Gewurztraminer**:

.. code-block:: bash

	/var/www/html/jbrowse/data/scripts/renamegenes.sh Riesling