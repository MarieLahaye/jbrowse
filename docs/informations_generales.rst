======================
Informations générales
======================

Hiérarchie des dossiers
-----------------------
Le répertoire du jbrowse est composé de fichiers très important comme le fichier de configuration général du jbrowse (``jbrowse.conf`` ou ``jbrowse_conf.json`` - mais le deuxième n'est pas utilisé dans notre cas), de nombreux dossiers, tels que ``plugins/`` qui contient tous les plugins installés ou ``src/`` dans lequel se trouve tout le code source. Le dossier ``data/``, lui, est celui dans lequel on ajoute toutes les données qui vont être intégrées au jbrowse. L'organisation de ce dossier est représentée ci-dessous:

.. code-block:: bash

	.
	├── Gewurztraminer
	│   ├── BAM_RNAseq
	│   ├── BED_RNAseq
	│   ├── functions.conf
	│   ├── Gewurztraminer_Ngaps.sorted.bed.gz
	│   ├── Gewurztraminer_Ngaps.sorted.bed.gz.tbi
	│   ├── Gewurztraminer_pseudomolecules.fa
	│   ├── Gewurztraminer_pseudomolecules.fa.fai
	│   ├── GW_annotations.sorted.gff3.gz
	│   ├── GW_annotations.sorted.gff3.gz.tbi
	│   ├── GW_metadata.json
	│   ├── names
	│   ├── trackList.json
	│   └── WIG_RNAseq
	├── Riesling
	│   ├── BAM_RNAseq
	│   ├── BED_RNAseq
	│   ├── contigs
	│   ├── functions.conf
	│   ├── names
	│   ├── RI_alignments_cds
	│   ├── RI_alignments_haplotigs
	│   ├── RI_annotations.sorted.gff3.gz
	│   ├── RI_annotations.sorted.gff3.gz.tbi
	│   ├── Riesling_Ngaps.sorted.bed.gz
	│   ├── Riesling_Ngaps.sorted.bed.gz.tbi
	│   ├── Riesling_pseudomolecules.fa
	│   ├── Riesling_pseudomolecules.fa.fai
	│   ├── RI_metadata.json
	│   ├── trackList.json
	│   └── WIG_RNAseq
	└── scripts
	    ├── add_name_bed.py
	    ├── add_note_gff.py
	    ├── bamtobigwig.sh
	    ├── bed_cds.py
	    ├── bedcds.sh
	    ├── bed_contigs.py
	    ├── bed_haplotigs.py
	    ├── bedhaplotigs.sh
	    ├── bedsashimi.sh
	    ├── coords_to_bed.py
	    ├── fasta_to_bed.py
	    ├── fc_coords2hp.py
	    ├── filter_coords.py
	    ├── indexbam.sh
	    ├── initialization_tracks.py
	    └── junctions_positions.py


Dans le dossier ``data/`` se trouve le répertoire ``scripts/`` qui contient tous les scripts python et bash qui pourront être utilisés pour gérer les tracks, formatter, filtrer et traiter les données afin que le contenu corresponde à ce qu'attend le jbrowse.

Ensuite, pour chaque génome étudiée, un répertoire doit être créé. Et, comme on le verra ensuite, il devra être appelé dans le fichier de configuration général afin qu'il soit pris en compte et visualisable. Dans notre jbrowse, les génomes du Riesling et du Gewurztraminer sont accessibles. Dans chaque répertoire, le fichier de configuration des tracks: ``trackList.json`` doit obligatoirement être présent. Il peut être créé et modifié à partir du script ``initialization_tracks.py``. Ce fichier de configuration peut, pour certains tracks, faire appel à des fonctions un peu complexes qui ont été définies dans le fichier ``functions.conf``. On y retrouve aussi le fichier contenant les métadonnées (par exemple: ``RI_metadata.json``), ainsi que toutes les données intégrées au jbrowse.


Nommage des dossiers/fichiers
-----------------------------
Pour mieux s'y retrouver dans toutes les données qui ont été ou vont être intégrées, il est recommandé de bien séparer les fichiers en fonction de leur format et des données qu'ils contiennent. Dans le cas de notre jbrowse, à partir du moment où une catégorie comprenait plusieurs fichiers de données, ceux-ci ont été placés dans un dossier particulier. Par exemple, les données de la catégorie ``Alignments`` du jbrowse, proviennent des fichiers d'alignements BAM obtenus après une analyse RNAseq, ils ont donc tous été regroupés dans le dossier ``BAM_RNAseq/``. Il est important de bien séparer les fichiers pour que le script ``initialization_tracks.py`` qui créé les tracks s'y retrouve et sache à quelle catégorie correspondent tous les types de données.


Configuration générale du jbrowse
---------------------------------


Configuration des tracks
------------------------
