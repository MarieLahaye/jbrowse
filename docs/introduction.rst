============
Introduction
============
Actuellement, version 1.16.9 du jbrowse. Génomes disponibles à la visualisation: Riesling et Gewurztraminer.

Formats de données supportés pour la génération automatiques des tracks via le script ``initialization_track.py``: BED, BAM, BigWig, FASTA, GFF3.

L'ensemble des exemples donnés dans cette documentation sont basés sur les données du Riesling, il est naturellement possible de les adapater avec n'importe lesquelles de vos données.


.. _installation:

Installation
------------
La version pour développeur, qui donne accès au code du jbrowse et la possibilité de le modifier, est accessible sur gitHub à l'adresse `https://github.com/GMOD/jbrowse <https://github.com/GMOD/jbrowse>`_. 
Pour l'installation, et la mise à jour, il suffit de cloner le dépôt git dans le répertoire ``/var/www/html/`` et lancer le script ``setup.sh``:

.. code-block:: bash

	git clone https://github.com/GMOD/jbrowse
	cd jbrowse
	./setup.sh

Dans le cas d'une mise à jour, il faudra ensuite déplacer les fichiers de configuration (.json ou .conf), scripts, plugins ajoutés, et les données contenues dans ``data/``, du dossier correspondant à l'ancienne version du jbrowse, vers le nouveau. Il faudra aussi reprendre à la main toutes les modifications qui auraient pu être faites dans le code du jbrowse.

Le jbrowse est alors accessible en entrant l'adresse: `http://147.100.144.109/jbrowse/ <http://147.100.144.109/jbrowse/>`_ dans la barre de recherche d'un navigateur web.

*Note*: Lors de l'importation ou de la création de nouveaux plugins, la commande ``./setup.sh`` sera systématiquement à relancer.


.. _modification code:

Modification du code
--------------------
Si l'envie vous prend de modifier le code de base du jbrowse, avant toute modification il faut lancer la commande:

.. code-block:: bash

	yarn watch

*Note*: Cette commande doit être lancée dans le terminal et tourner pendant tout le temps des changements effectués dans le code. Elle va surveiller toute modification qui sera faite et les inclure au jbrowse.


.. _plugins:

Ajout de plugins
----------------
Pour ajouter un plugin (trouvé sur gitHub par exemple), il vous suffit de cloner le dépôt git dans le répertoire ``/var/www/html/jbrowse/plugins/``, puis de lancer une commande afin de 'construire' le code du plugin dans le jbrowse. Prenons comme exemple l'installation du plugin ``Sashimi Plot``:

.. code-block:: bash

	git clone https://github.com/elsiklab/sashimiplot.git
	./setup.sh

Le plugin maintenant installé, il faut l'activer en l'intégrant au fichier de configuration ``/var/www/html/jbrowse/jbrowse.conf``, par exemple dans le cas du plugin pour représenter les sashimis plots:

.. code-block:: bash

	[plugins.SashimiPlot]
	location = plugins/SashimiPlot