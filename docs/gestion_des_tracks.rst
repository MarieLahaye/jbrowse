==================
Gestion des tracks
==================

Script ``initialization_track.py``
----------------------------------
Le script ``initialization_track.py`` situé dans le dossier ``/var/www/html/data/scripts/``, permet la génération de tracks dans les fichiers de configuration ``trackList.json``. Mais aussi, de supprimer ou remplacer certains tracks, en fonction de leur nom ou du chemin vers le fichier concerné. La commande de base (la plus simple), est utilisée pour générer automatiquement des tracks en indiquant :

	-c, --conf jsonFile				Fichier de configuration des tracks trackList.json. Note: Cette option est toujours requise
	-a, --add dataFilesList  		Liste des fichiers contenant les données qui doivent être ajoutées au jbrowse

Plusieurs autres options sont disponibles:

	-m, --metadata metadataFile		fichier metadata.json pour ajouter des metadonnées dans les tracks correspondant aux fichiers indiqués avec l'option ``-a``
	-r, --remove keyList			nom des tracks (ou *key*) à supprimer
	--replace 						remplacer les metadonnées si elles existent déjà pour les fichiers indiqués
	--update						mise à jour des tracks donnés dans la commande 

Exemples de commandes
~~~~~~~~~~~~~~~~~~~~~

Ajouter des metadonnées:

.. code-block:: bash

	/var/www/html/data/scripts/initialization_track.py -c trackList.json -a BAM_RNAseq/*.bam -m RI_metadata.json

Mettre à jour certains tracks:

.. code-block:: bash

	/var/www/html/data/scripts/initialization_track.py -c trackList.json -a BAM_RNAseq/*.bam --update

Supprimer des tracks:

.. code-block:: bash

	/var/www/html/data/scripts/initialization_track.py -c trackList.json -r RI_S1_571 RI_S2_576 RI_S3_578

Fichier ``metadata.json``
-------------------------
Les metadonnées étant spécifiques à chaque track, il faut les définir dans un autre fichier JSON, que l'on peut nommer par exemple: "RI_metadata.json", dans le cas du Riesling.

.. A faire + tard car l'insertion des metadonnées va un peu changer ces prochaines jours 23/09/2020 
