#!/usr/bin/bash

directory=$1

if [ "${directory}" != "Riesling" ] && [ "${directory}" != "Gewurztraminer" ]; then
    echo "Argument must be 'Riesling' or 'Gewurztraminer'"
    exit 1
fi

if [ "$directory" == "Riesling" ]; then
	sygle="RI"
else
	sygle="GW"
fi

cd /data2/malahaye/data_jbrowse/${directory}/alt_data_filtered

# Liens symboliques vers les fichiers originaux #
dir1=ori_files
if [ ! -d "${dir1}" ]; then
    mkdir ${dir1}
fi

ln -fs /data2/malahaye/data_jbrowse/${directory}/2020_annotations_${sygle}_ALT/outdir.${sygle}_ALT/sequences.gff3 ${dir1}/sequences.gff3
ln -fs /data2/malahaye/data_jbrowse/${directory}/copie_assemblage/${directory}_haplotigs.fa ${dir1}/${directory}_haplotigs.fa
ln -fs /data2/malahaye/data_jbrowse/${directory}/2020_annotations_${sygle}_ALT/outdir.${sygle}_ALT/sequences_cds.fna ${dir1}/sequences_cds.fna
ln -fs /data2/malahaye/data_jbrowse/${directory}/2020_annotations_${sygle}_ALT/outdir.${sygle}_ALT/sequences_gene.fna ${dir1}/sequences_gene.fna
ln -fs /data2/malahaye/data_jbrowse/${directory}/2020_annotations_${sygle}_ALT/outdir.${sygle}_ALT/sequences_mrna.fna ${dir1}/sequences_mrna.fna
ln -fs /data2/malahaye/data_jbrowse/${directory}/2020_annotations_${sygle}_ALT/outdir.${sygle}_ALT/sequences_ncrna.fna ${dir1}/sequences_ncrna.fna
ln -fs /data2/malahaye/data_jbrowse/${directory}/2020_annotations_${sygle}_ALT/outdir.${sygle}_ALT/sequences_prot.fna ${dir1}/sequence_prot.fna
ln -fs /data2/malahaye/data_jbrowse/${directory}/alignments_haplotigs/haplotigs_phase_0.bed

# Check si le répertoire filter_phase_0 existe #
dir2=filter_phase_0
if [ ! -d "${dir2}" ]; then
    mkdir ${dir2}
fi

# D'abord on récupère tous les haplotigs dont les séquences n'ont pas à être filtrées (phase 1) puis on ajoute les phase O filtrés #
bedtools getfasta -fi ${dir1}/${directory}_haplotigs.fa -bed haplotigs_phase_0.bed | sed "s/:.*//g" > ${dir2}/${directory}_haplotigs.filtered.fa
echo "Filtering ${directory}_haplotigs.filtered.fa..."
samtools faidx ${dir1}/${directory}_haplotigs.fa `grep ">" ${dir1}/${directory}_haplotigs.fa | grep -v "_phase_0" | sed "s/>//"` >> ${dir2}/${directory}_haplotigs.filtered.fa

# Suppression du fichier indexé inutile maintenant #
rm ${dir1}/${directory}_haplotigs.fa.fai

# Génération d'un BED contenant uniquement les gènes appartenant aux haplotis phase 0 à partir du fichier GFF #
/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/gff_to_bed.py -i ${dir1}/sequences.gff3 -o genes_phase_0.bed -id phase_0

# Intersect entre les gènes phase 0 et les haplotigs phase 0 filtrés #
# On récupère uniquement la liste des gènes qui sont retrouvés sur les haplotigs phase 0 filtrés #
sortBed -i genes_phase_0.bed > genes_phase_0.sorted.bed
sortBed -i haplotigs_phase_0.bed > haplotigs_phase_0.sorted.bed
bedtools intersect -a genes_phase_0.sorted.bed -b haplotigs_phase_0.sorted.bed -wa -wb | cut -f4 | sort | uniq > genes_phase_0_filtered.txt

# Filtre des fna et du gff #
for i in ${dir1}/*.{fna,gff3}
do
    file=${i##*/}
    file_name=${file%%.*}
    extension=${file##*.}
    echo "Filtering ${i}..."
    /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/formatting_fna_gff.py filter -i ${dir1}/${file} -g genes_phase_0_filtered.txt -o ${dir2}/${file_name}.filtered.${extension}
done
