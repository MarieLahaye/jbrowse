#!/usr/bin/env python3
# coding: utf-8
import argparse
import pandas as pd


def command_line():
    parser = argparse.ArgumentParser(description='''
        Generate BED of genes from GFF file 
        Optionnal: select lines by giving a list of IDs or part of gene IDs
        ''')
    parser.add_argument('-i', '--input', required=True, help='GFF file')
    parser.add_argument('-id', '--listID', nargs='+', 
                        help='List of IDs or part of gene IDs')
    parser.add_argument('-o', '--output', required=True, help='BED file')
    args = parser.parse_args()
    return args.input, args.listID, args.output


def convert(finput, list_id, foutput):
    gff_df = pd.read_csv(finput, sep='\t', header=None, comment="#")

    # récupération de toutes les lignes correspondant à un gène
    genes_df = gff_df[gff_df[2] == "gene"]

    lines = []
    # si on a donné une liste de gènes en entrée ou une information plus ...
    # ... générale contenue dans l'id comme "phase_0" pour ne récupérer ...
    # ... que les gènes localisés sur des haplotigs phase 0
    if list_id:
        for i in list_id:
            gene = genes_df[genes_df[8].str.contains(i)].values
            for j in gene:
                gene_id = [g[8:] for g in j[8].split(';') if "ID" in g]
                line = [j[0], j[3], j[4], gene_id[0]]
                lines.append(line)
    else:
        for i in range(0, len(genes_df)):
            gene = genes_df.iloc[i, :]
            # ajoute à la ligne le nom du gène
            gene_id = [g[8:] for g in gene[8].split(';') if "ID" in g]
            line = [gene[0], gene[3], gene[4], gene_id[0]]
            lines.append(line)

    # écriture du bed
    output_df = pd.DataFrame(lines)
    output_df.to_csv(foutput, sep='\t', header=False, index=False)


if __name__ == "__main__":
    finput, list_id, foutput = command_line()
    convert(finput, list_id, foutput)

