# si dossier existe
cd /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/alignments_haplotigs/

export PATH=/cm/shared/apps/miniconda3/envs/falcon-phase/bin/:$PATH
export PATH=/cm/shared/apps/pb-falcon-phase/bin/:$PATH

# séparation du fasta des haplotigs en 2: phase 0 et phase 1
#samtools faidx Riesling_haplotigs.fa `grep "_phase_0" Riesling_haplotigs.fa | sed "s/>//"` > Riesling_haplotigs_phase_0.fa
#samtools faidx Riesling_haplotigs.fa `grep ">" Riesling_haplotigs.fa | grep -v "_phase_0" | sed "s/>//"` > Riesling_haplotigs_phase_1.fa

# alignement des haplotigs sur les pseudomolécules et afficher blocs + % identité
# colorer les blocs selon l'identité

# liens symboliques vers les fichiers fasta
ln -s /data2/malahaye/data_jbrowse/Riesling/copie_assemblage/Riesling_pseudomolecules_ctgP.fa
ln -s /data2/malahaye/data_jbrowse/Riesling/copie_assemblage/Riesling_haplotigs.fa

# alignement avec nucmer des haplotigs sur la pseudomolécule
nucmer --prefix RI_haplotigs_alignments --maxmatch -l 100 -c 500 Riesling_pseudomolecules_ctgP.fa Riesling_haplotigs.fa

delta-filter -g RI_haplotigs_alignments.delta > RI_haplotigs_alignments.delta.filter

show-coords -qTlHL 5000 RI_haplotigs_alignments.delta.filter > RI_haplotigs_alignments.coords

cat RI_haplotigs_alignments.coords | sort -k11,11 -k1,1n  > RI_haplotigs_alignments.coords.out

# génère bed pour ensuite pouvoir comparer avec les contigs et déterminer quels alignements sont bons
# ils le sont si l'alignement se trouve dans les bornes du contig primaire correspondant à l'haplotig aligné
/var/www/html/jbrowse/data/scripts/coords_to_bed.py -i RI_haplotigs_alignments.coords.out -o haplotigs_coords.bed
sortBed -i haplotigs_coords.bed > haplotigs_coords.sorted.bed

# si contigs pas dans le dossier 
cp ../contigs/contigs.bed .
sortBed -i contigs.bed > contigs.sorted.bed

# intersect pour voir quels alignements sont sur quel contig primaire et s'ils correspondent au bon haplotig
# -wa et -wb pour afficher les coordonnées d'origine des 2 entrées
bedtools intersect -a haplotigs_coords.sorted.bed -b contigs.sorted.bed -wa -wb > intersect.tab

# filtre du .coords à partir du fichier intersect.tab pour ne garder que les alignements des haplotigs au niveau ...
# ... contig primaire correspondant
/var/www/html/jbrowse/data/scripts/filter_coords.py -c RI_haplotigs_alignments.coords.out -i intersect.tab -o RI_haplotigs_alignments_filter.coords.out

# récupère aussi maintenant le % d'identité des alignements chaque haplotig
/var/www/html/jbrowse/data/scripts/fc_coords2hp.py RI_haplotigs_alignments_filter.coords.out | sort -k6,6 -k8,8n > RI_haplotigs_alignments_filter.hp.txt

# génération du .bed contenant les positions des haplotigs sur les chromosomes et le pourcentage d'identité, et filtre des portions qui étaient manquantes ...
# ... et qui ne sont pas localisées sur le bon contig primaire
/var/www/html/jbrowse/data/scripts/bed_haplotigs.py -i RI_haplotigs_alignments_filter.hp.txt -c contigs.sorted.bed -o haplotigs.bed

# Etapes suivantes non nécessaires
# filtre des placements
#/cm/shared/apps/pb-falcon-phase/bin/fc_filt_hp.py RI_haplotigs_alignments.hp.txt > RI_haplotigs_alignments.filt_hp.txt

# alignement gene ref vs gene alt, faire des blocs de gène comme https://phytozome.jgi.doe.gov/jbrowse/, et quand on clique on affiche les différences
# colorer selon l'identité
