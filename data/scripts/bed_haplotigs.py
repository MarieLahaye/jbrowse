#!/usr/bin/env python3
# coding: utf-8
import argparse
from pybedtools import BedTool
import pandas as pd

def command_line():
    parser = argparse.ArgumentParser(description='''
        Generate BED file from .txt file containing positions
        and identity percentage informations. Writing of an 
        additional phase_0.bed file which contains the coordinates
        of the heterozygous portions of the phase_0 haplotigs
        ''')
    parser.add_argument('-i', '--input', required=True, help='TXT file')
    parser.add_argument('-o', '--output', required=True, help='BED file')
    parser.add_argument('-c', '--contigs', required=True, help='BED contigs coordinates')
    args = parser.parse_args()
    return args.input, args.output, args.contigs


def bed_phase_0(phase_0):
    phase_0_file = open('haplotigs_phase_0.bed', 'w')
    for haplotig in phase_0:
        phase_0_file.write('\t'.join(haplotig)+'\n')
    phase_0_file.close()


def create_bed_lists(input):
    with open(input, 'r') as input_file:
        phase_0_list = []
        haplotigs_list = []
        # Parcours des lignes du fichier et création d'une liste contenant les éléments ...
        # ... retrouvés dans un fichier BED 
        for line in input_file:
            line_list = line[:-1].split('\t')
            # Liste pour écrire la ligne du fichier BED
            bed_line_list = [line_list[i] for i in [5, 7, 8, 0, 12]]
            # Liste pour récupérer les coordonnées des portions de phase 0 qui sont ...
            # ... représentés sur le Jbrowse de la séquence de référence
            if "phase_0" in line_list[0]:
                phase_0_list.append([line_list[j] for j in [0, 2, 3]])
            haplotigs_list.append(bed_line_list)

    # Ecriture du fichier BED contenant les coordonnées de haplotigs phase_0
    bed_phase_0(phase_0_list)
    return haplotigs_list, phase_0_list


# Création d'un objet bedtool à partir d'une liste ou d'un fichier BED
def create_bedtool_object(bed):
    return BedTool(bed)


# Fonction permettant de filtrer les intersections pour lesquelles l'identifiant ...
# .. de l'haplotig et celui du contig primaire ne correspondent pas
def filter_wrong_alignements(feature):
    feature_list = list(feature)
    if feature_list[8][:-1] in feature_list[3]:
        return True
    return False


# Intersect sur les haplotigs et les contigs primaires, les colonnes 2 et 3 correspondent ...
# ... aux coordonnées de l'intersection entre l'haplotig et le contig primaire (cela nous ...
# ... donne les coordonnées de la portion d'haplotig aligné uniquement sur sont contig primaire)
def intersect(haplotigs, contigs):
    # Création objets bedtool
    bed_haplotigs = create_bedtool_object(haplotigs)
    bed_contigs = create_bedtool_object(contigs)

    # Intersect et filtre
    h_intersect_c = bed_haplotigs.intersect(bed_contigs, wb=True).filter(filter_wrong_alignements)
    return h_intersect_c


# Fonction pour conserver uniquement certaines colonnes en se basant sur une liste d'indices ...
# ... donnée en entrée
def select_columns(feature, columns):
    return [list(feature)[c] for c in columns]


# Ecriture du fichier BED de sortie
def write_output(intersect, foutput):
    columns_list = [0, 1, 2, 3, 4]
    intersect.each(select_columns, columns_list).saveas(foutput)



if __name__ == "__main__":
    finput, foutput, fcontigs = command_line()
    haplotigs_list, phase_0_list = create_bed_lists(finput)
    h_intersect_c = intersect(haplotigs_list, fcontigs)
    write_output(h_intersect_c, foutput)
