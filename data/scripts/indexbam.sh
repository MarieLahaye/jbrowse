#!/bin/bash

directory=$1

if [ "${directory}" != "Riesling" ] && [ "${directory}" != "Gewurztraminer" ]; then
    echo "Argument must be 'Riesling' or 'Gewurztraminer'"
    exit 1
fi

cd /data2/malahaye/jbrowse_data/${directory}/BAM_RNAseq

for i in *.bam
do
	echo "Indexing $i..."
	samtools index $i
done
