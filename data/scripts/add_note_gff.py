#!/usr/bin/env python3
# coding: utf-8
import argparse
import re


def command_line():
    parser = argparse.ArgumentParser(description='''
        Add tRNA or ncRNA note in GFF file
        ''')
    parser.add_argument('-i', '--input', required=True, help='GFF file')
    parser.add_argument('-o', '--output', required=True, help='Output file')
    args = parser.parse_args()
    return args.input, args.output


fInput, fOutput = command_line()
input_file = open(fInput, 'r')
output_file = open(fOutput, 'w')
for line in input_file:
    if not re.search('^#', line):
        if line.split('\t')[2] == "tRNA":
            line = line[:-1]+";Note=tRNA\n"
        elif line.split('\t')[2] == "ncRNA":
            line = line[:-1]+";Note=ncRNA\n"
    output_file.write(line)

input_file.close()
output_file.close()
