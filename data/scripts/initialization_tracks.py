#!/usr/bin/env python3
# coding: utf-8
import argparse
import json
import os.path


# Fonction pour définir tous les arguments et options de la ligne de commande
def command_line():
    parser = argparse.ArgumentParser(description='''
        Tracks management of the configuration file trackList.json. Creation,
        update, deletion, addition of metadata.
        ''')
    parser.add_argument(
        '-c', '--conf', required=True,
        help='trackList.json - where the tracks will be added'
        )
    parser.add_argument(
        '-a', '--add', nargs='+',
        help='''
        list of files to add to the jbrowse/list of files for
        which we want to add metadata
        '''
        )
    parser.add_argument(
        '-r', '--remove', nargs='+',
        help='list of tracks label to remove from the jbrowse')
    parser.add_argument(
        '-m', '--metadata',
        help='JSON file with metadata for specific tracks')
    parser.add_argument(
        '--replace', action='store_true',
        help='to replace metadata')
    parser.add_argument(
        '--update', action='store_true',
        help='to update existing tracks by giving the files path')
    args = parser.parse_args()
    return args.conf, args.add, args.remove,\
        args.metadata, args.replace, args.update


# Récupération du nom du fichier bam sans extension, de l'extension ...
# ... du dossier parent et du chemin du fichier à mettre dans l'url du tracks
def path_name(file, json_path):
    name = os.path.basename(file).split('.')[0]
    extension = os.path.basename(file).split('.')[-1]
    if extension == "gz":
        extension = os.path.basename(file).split('.')[-2]
    file_path = file.replace(
        os.path.commonprefix(
            [file, json_path]
        ), "")
    directory = os.path.dirname(file).split('/')[-1]
    if not directory:
        absolute_path = os.path.abspath(file)
        directory = os.path.dirname(absolute_path).split('/')[-1]
    return name, extension, file_path, directory


# Suppression track en fonction de son label, ou de son chemin --> dans le...
# ... cas où l'on veut mettre à jour les tracks
def remove_track(name, data, update, file_path=None):
    for track in data['tracks'][:]:
        if update:
            if track['urlTemplate'] == file_path:
                data['tracks'].remove(track)
                break
        else:
            if track['key'] == name:
                data['tracks'].remove(track)


# Fonction pour regrouper les tracks en fonction de la catégorie à laquelle...
# ... ils sont associés
def tracks_list(data):
    trackList = {
        "BAM_RNAseq": [],
        "Annotations": [],
        "BigWig": [],
        "Reference sequence": [],
        "Missing sequences": [],
        "SashimiPlot": [],
        "Haplotigs": [],
        "Contigs": [],
        "Allele": []
    }
    for t in data['tracks']:
        if "Alignments" in t['category']:
            trackList['BAM_RNAseq'].append(t)
        elif "BigWig" in t['category']:
            trackList['BigWig'].append(t)
        elif "Annotations" in t['category']:
            trackList['Annotations'].append(t)
        elif "Reference sequence" in t['category']:
            trackList['Reference sequence'].append(t)
        elif "Missing sequences" in t['category']:
            trackList['Missing sequences'].append(t)
        elif "Sashimi plot" in t['category']:
            trackList['SashimiPlot'].append(t)
        elif "Haplotigs" in t['key']:
            trackList['Haplotigs'].append(t)
        elif "Contigs" in t['category']:
            trackList['Contigs'].append(t) 
        elif "Allele" in t['key']:
            trackList['Allele'].append(t)
    return trackList


# Ajout/Remplacement metadata et création des tracks
def track(data, metadata_file, directory, file_path, name, replace,
          extension, trackList, replace_message, update_message):
    if extension == "fa":
        if not trackList['Reference sequence']:
            create_refseq_track(file_path, data)
        else:
            category = "Reference sequence"
            update_message = upd_message(name, category, update_message)
    elif extension == "bam":
        if directory == "merged_RNAseq":
            name = "Merged RNAseq data"
        if name not in [t['key'] for t in trackList['BAM_RNAseq']]:
            create_bam_track(name, directory, file_path, data)
        else:
            category = "Alignments"
            update_message = upd_message(name, category, update_message)
    elif extension == "bw":
        if directory == "merged_RNAseq":
            name = "Merged RNAseq data"
        if name not in [t['key'] for t in trackList['BigWig']]:
            create_wig_track(name, file_path, data)
        else:
            category = "BigWig Density and XY"
            update_message = upd_message(name, category, update_message)
    elif extension == "gff3":
        if not trackList['Annotations']:
            create_gff_track(directory, file_path, data)
        else:
            category = "Annotations"
            update_message = upd_message(name, category, update_message)
    elif extension == "bed":
        if directory == "BED_RNAseq":
            if name not in [t['key'] for t in trackList['SashimiPlot']]:
                create_sashimi_track(name, directory, file_path, data)
            else:
                category = "Sashimi plot"
                update_message = upd_message(name, category, update_message)
        elif directory == "merged_RNAseq":
            name = "Merged RNAseq data"
            if name not in [t['key'] for t in trackList['SashimiPlot']]:
                create_sashimi_track(name, directory, file_path, data)
            else:
                category = "Sashimi plot"
                update_message = upd_message(name, category, update_message)
        elif "alignments_haplotigs" in directory:
            if not trackList['Haplotigs']:
                create_haplotigs_track(directory, file_path, data)
            else:
                category = "Haplotigs"
                update_message = upd_message(name, category, update_message)
        elif "alignments_cds" in directory:
            if not trackList['Allele']:
                create_allele_track(directory, file_path, data)
            else:
                category = "Allele"
                update_message = upd_message(name, category, update_message)
        elif directory == "contigs":
            if not trackList['Contigs']:
                create_contigs_track(directory, file_path, data)
            else:
                category = "Contigs"
                update_message = upd_message(name, category, update_message)
        else:
            if not trackList['Missing sequences']:
                create_n_track(directory, file_path, data)
            else:
                category = "Missing sequences"
                update_message = upd_message(name, category, update_message)

    for d in data['tracks']:
        if name in d['key'] and metadata_file:
            if extension == "bam":
                if "Alignments" in d['category']:
                    replace_message = metadata(replace, metadata_file,
                                           d, name, replace_message)
            elif extension == "bed":
                if "Sashimi plot" in d['category'] and directory == "BED_RNAseq":
                    replace_message = metadata(replace, metadata_file,
                                                    d, name, replace_message)
            elif extension == "bw":
                if "BigWig" in d['category']:
                    replace_message = metadata(replace, metadata_file,
                                               d, name, replace_message)
    return replace_message, update_message


def create_base_config():
    track = {
        "formatVersion": 1,
        "include": ["functions.conf"],
        "names": {
            "type": "Hash",
            "url": "names/"
        },
        "tracks": []
    }
    json.dump(track, json_file, indent=4)


def create_refseq_track(path, data):
    track = {
        "category": "Reference sequence",
        "chunkSize": 20000,
        "key": "Reference sequence",
        "label": "DNA",
        "seqType": "dna",
        "storeClass": "JBrowse/Store/SeqFeature/IndexedFasta",
        "type": "SequenceTrack",
        "urlTemplate": path
    }
    data['tracks'].append(track)
    data['refSeqs'] = "{}.fai".format(path)


# Création des tracks pour les fichiers gff
def create_gff_track(directory, path, data):
    track = {
        "category": "Annotations",
        "indexedFeatures": [
            "gene",
            "mRNA",
            "exon"
        ],
        "key": "Gene annotations",
        "label": "{}_gene_annotations".format(directory),
        "labelTranscripts": False,
        "nameAttributes": [
            "ID",
            "Name"
        ],
        "onClick": {
            "label" : "{subfeatureType}",
            "title" : "{name}",
            "action": "defaultDialog"
        },
        "storeClass": "JBrowse/Store/SeqFeature/GFF3Tabix",
        "style": {
            "description": "{subfeatureDescription}",
            "showTooltips": False
        },
        "type": "CanvasFeatures",
        "urlTemplate": path
    }
    data['tracks'].append(track)


# Création des tracks pour les fichiers bam
def create_bam_track(name, directory, path, data):
    track = {
        "category": "Alignments",
        "key": name,
        "label": "{0}_{1}_alignements".format(name, directory),
        "storeClass": "JBrowse/Store/SeqFeature/BAM",
        "type": "Alignments2",
        "urlTemplate": path
    }
    data['tracks'].append(track)


# Création des tracks pour les fichiers wig
def create_wig_track(name, path, data):
    track1 = {
        "autoscale": "local",
        "category": "BigWig Density",
        "key": name,
        "label": name+"_bigwig_density",
        "storeClass": "JBrowse/Store/SeqFeature/BigWig",
        "type": "JBrowse/View/Track/Wiggle/Density",
        "urlTemplate": path,
        "variance_band": True
    }
    track2 = {
        "autoscale": "local",
        "category": "BigWig XY",
        "key": name,
        "label": name+"_bigwig_xy",
        "storeClass": "JBrowse/Store/SeqFeature/BigWig",
        "type": "JBrowse/View/Track/Wiggle/XYPlot",
        "urlTemplate": path
    }
    data['tracks'].extend((track1, track2))


def create_n_track(directory, path, data):
    track = {
        "category": "Missing sequences",
        "key": "N gaps",
        "label": "{}_missing_sequences".format(directory),
        "onClick": {
            "label": "{featureLength}",
            "title": "{name}",
            "action": "defaultDialog"
        },
        "storeClass": "JBrowse/Store/SeqFeature/BEDTabix",
        "style": {
            "showLabels": False,
            "color": "silver"
        },
        "type": "CanvasFeatures",
        "urlTemplate": path
    }
    data['tracks'].append(track)


def create_sashimi_track(name, directory, path, data):
    track = {
        "category": "Sashimi plot",
        "key": name,
        "label": "{0}_{1}_sashimi".format(name.replace(" ", "_"), directory),
        "storeClass": "JBrowse/Store/SeqFeature/BEDTabix",
        "type": "SashimiPlot/View/Track/Sashimi",
        "urlTemplate": path,
        "chunkSizeLimit": 50000000,
        "style": {
            "label": "{readsNb}",
            "showLabels": False
        }
    }
    data['tracks'].append(track)


def create_haplotigs_track(directory, path, data):
    track = {
        "category": "Haplotigs",
        "key": "haplotigs".capitalize(),
        "label": "{}".format(directory),
        "onClick": {
            "label": "{featureIdentity}",
            "title": "{name}",
            "action": "defaultDialog"
        },
        "storeClass": "JBrowse/Store/SeqFeature/BEDTabix",
        "style": {
            "color": "{featureColor}"
        },
        "type": "CanvasFeatures",
        "urlTemplate": path
    }
    data['tracks'].append(track)


def create_allele_track(directory, path, data):
    track = {
        "category": "Haplotigs",
        "key": "Allele",
        "label": "{}".format(directory),
        "onClick": {
            "label": "{featureIdentity}",
            "title": "{name}",
            "action": "defaultDialog"
        },
        "storeClass": "JBrowse/Store/SeqFeature/BEDTabix",
        "style": {
            "color": "{cdsAltColor}"
        },
        "type": "CanvasFeatures",
        "urlTemplate": path
    }
    data['tracks'].append(track)


def create_contigs_track(directory, path, data):
    track = {
        "category": "Contigs",
        "key": "contigs".capitalize(),
        "label": "{}".format(directory),
        "storeClass": "JBrowse/Store/SeqFeature/BEDTabix",
        "type": "CanvasFeatures",
        "urlTemplate": path
    }
    data['tracks'].append(track)


# Décide s'il faut ajouter, remplacer les métadonnées ou renvoyer un...
# ... message indiquant qu'elles existent déjà
def metadata(replace, metadata_file, d, name, replace_message):
    if 'metadata' in d.keys() and replace is True:
        add_metadata(metadata_file, d, name)
    elif 'metadata' in d.keys() and replace is False:
        replace_message = repl_message(name, d, replace_message)
    else:
        add_metadata(metadata_file, d, name)
    return replace_message


# Ajout metadata contenues dans le fichier metadata.json
def add_metadata(metadata_file, d, name):
    with open(metadata_file) as metadata:
        meta = json.load(metadata)
        d['metadata'] = {"description": meta[name]}


# Stockage dans une liste, des messages à renvoyer si un track existe déjà...
# ... dans le cas où l'option '--update' n'a pas été indiquée
def upd_message(name, category, message):
    if message:
        message.append(
            "{0} track in {1} category already exists".format(name, category)
        )
    else:
        message = [
            "{0} track in {1} category already exists".format(name, category)
        ]
    return message


# Stockage dans une liste, des messages à renvoyer si les metadata existent...
# ... pour des tracks donnés dans le cas où l'option '--replace' n'a pas...
# ... été indiquée dans la ligne de commande
def repl_message(name, d, message):
    if message:
        message.append(
            "Metadata already exists for {0} track in"
            " {1} category.".format(name, d['category'])
        )
    else:
        message = [
            "Metadata already exists for {0} track in"
            " {1} category.".format(name, d['category'])
        ]
    return message


json_path, create_list, remove_list, metadata_file,\
    replace, update = command_line()
update_message = None
replace_message = None

if __name__ == "__main__":
    # Gestion des exceptions:
    # Except permet de traiter le cas où l'erreur suivante est levée...
    # ... ici il s'agit du cas où le fichier json fournit n'est pas valide...
    # ... par exemple s'il est vide ou qu'il n'existe pas
    try:
        with open(json_path) as json_file:
            data = json.load(json_file)
            pass
    except (json.decoder.JSONDecodeError, FileNotFoundError):
        with open(json_path, 'w') as json_file:
            create_base_config()

    # Load le fichier json puis regarde quelle action réaliser : ...
    # ... suppression de tracks, création de tracks, récupération du...
    # ... nom ou du chemin du fichier, etc
    with open(json_path) as json_file:
        data = json.load(json_file)
        if remove_list:
            for name in remove_list:
                remove_track(name, data, update)

        if create_list:
            for file in create_list:
                name, extension, file_path, directory = path_name(
                    file, json_path
                )
                if update is True:
                    remove_track(name, data, update, file_path)
                trackList = tracks_list(data)
                replace_message, update_message = track(
                    data, metadata_file, directory,
                    file_path, name, replace, extension,
                    trackList, replace_message, update_message
                )

    if replace_message:
        for m in replace_message:
            print(m)
        print("Note: If you want to replace these metadata,"
              " please use the option '--replace'.\n")
    if update_message:
        for m in update_message:
            print(m)
        print("Note: If you want to update these tracks,"
              " please use the option '--update'.\n")

    with open(json_path, 'w') as json_file:
        json.dump(data, json_file, indent=4)
