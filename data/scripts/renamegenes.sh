#!/usr/bin/bash

# check si l'argument donnée correspond
directory=$1

if [ "${directory}" != "Riesling" ] && [ "${directory}" != "Gewurztraminer" ]; then
    echo "Argument must be 'Riesling' or 'Gewurztraminer'"
    exit 1
fi

cd /data2/malahaye/data_jbrowse/${directory}/alt_data_filtered

# lien symbolique vers le fichier contenant les correspondances entres les gènes alt et ref
ln -fs /data2/malahaye/data_jbrowse/$directory/alignments_cds/genes_list.tsv

dir1=filter_phase_0
dir2=renamed
if [ ! -d "${dir2}" ]; then
    mkdir ${dir2}
fi

# Renommer les gènes à partir des correspondances cds alt/ref
for i in ${dir1}/*.{fna,gff3}
do
    file=${i##*/}
    file_name=${file%%.*}
    extension=${file##*.}
    echo "Rename $i..."
    /var/www/html/jbrowse_dev/data/scripts/formatting_fna_gff.py rename -i ${dir1}/${file} -g genes_list.tsv -o ${dir2}/${file_name}.rn.${extension}
done
