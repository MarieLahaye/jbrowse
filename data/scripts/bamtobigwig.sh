#!/usr/bin/bash

# check l'argument de la ligne de commande
directory=$1

if [ "${directory}" != "Riesling" ] && [ "${directory}" != "Gewurztraminer" ]; then
    echo "Argument must be 'Riesling' or 'Gewurztraminer'"
    exit 1
fi

# récupère dans une variable le sygle RI ou GW en fonction du répertoire 
if [ "${directory}" == "Riesling" ]; then
	sygle="RI"
elif [ "${directory}" == "Gewurztraminer" ]; then
	sygle="GW"
fi

cd /data2/malahaye/data_jbrowse/data/${directory}

cut -f1,2 ${directory}_pseudomolecules_ctgP.fa.fai > ${sygle}.chrom.sizes

cd WIG_RNAseq/
ln -s /data2/malahaye/data_jbrowse/data/${directory}/${sygle}.chrom.sizes

module load wigToBigWig/1.0
for i in /data2/malahaye/data_jbrowse/data/${directory}/BAM_RNAseq/*.bam
do
	file=${i##*/}
	file_name=${file%%.*}
	echo "Bam to Big Wig $i..."
	bam2wig -t ${file_name} $i > ${file_name}.wig
	wigToBigWig ${file_name}.wig ${sygle}.chrom.sizes ${file_name}.bw
done
