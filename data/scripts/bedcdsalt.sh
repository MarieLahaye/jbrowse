cd /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/alignments_cds/

# lien symbolique ou copie des fichiers sequences_cds.fna (ALT et REF) et du haplotigs.bed
ln -s /data2/malahaye/data_jbrowse/Riesling/alt_data_filtered/filter_phase_0/sequences_cds.filtered.fna sequences_cds_ref.fna
ln -s /data2/malahaye/data_jbrowse/Riesling/2020_annotations_RI_ALT/outdir.RI_ALT/sequences_cds.fna sequences_cds_alt.fna
ln -s /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/alignments_haplotigs/haplotigs.bed haplotigs.bed
ln -s /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/contigs/contigs.bed
ln -s /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/RI_annotations.ori.gff3 RI_annotations.gff3

# génération bdd CDS dans le dossier CSD_db
makeblastdb -in sequences_cds_ref.fna -dbtype nucl -out sequences_cds_db -hash_index -title CDS_database
mkdir CDS_db
mv sequences_cds_db* CDS_db/

# alignement des cds alt contre cds ref
blastn -db CDS_db/sequences_cds_db -query sequences_cds_alt.fna -out cds_align.blast -outfmt 6

# remplace les espace de l'id du fasta par: "|"
sed -i "s/ /|/g" sequences_cds_ref.fna

# génération bed pour les cds de référence
/var/www/html/jbrowse_dev/data/scripts/fasta_to_bed.py -i sequences_cds_ref.fna -o cds_ref.bed

# sort bed contigs primaires et cds_ref
sortBed -i contigs.bed > contigs.sorted.bed
sortBed -i cds_ref.bed > cds_ref.sorted.bed

# intersect sur les contigs primaires et les cds ref pour trouver les correspondances
bedtools intersect -a cds_ref.sorted.bed -b contigs.sorted.bed -wa -wb > intersect.tab

# récupère les lignes du gff correspondant uniquement aux gènes
#grep -P "gene\t" /data2/malahaye/data_jbrowse/Riesling/RI_annotations.gff3 > RI_genes.gff3

# fasta to bed pour ne récupérer que les ids ainsi que leurs positions
#/var/www/html/jbrowse_dev/data/scripts/fasta_to_bed.py -i sequences_gene.fna -o gene_ref.bed

# filtre des alignements pour ne garder que les cds alignés par rapport à leurs bons ...
# ... cds de référence et donc leurs bons contigs primaires #
/var/www/html/jbrowse_dev/data/scripts/bed_cds_alt.py -b cds_align.blast -i intersect.tab -r RI_annotations.gff3 -H haplotigs.bed -o cds_alt.bed