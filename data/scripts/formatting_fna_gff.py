#!/usr/bin/env python3
# coding: utf-8
import argparse
import fastaparser
import gffutils
import pandas as pd
import os
import sqlite3
import warnings

# on ignore le warning "RessourceWarning : unclosed file" ...
# ... qui est dû au module "gffutils" qui ne ferme pas ...
# ... le ficher à partir duquel la bdd est créée
warnings.filterwarnings('ignore', category=ResourceWarning)


def command_line():
    parser = argparse.ArgumentParser(description='''
        Filter phase 0 or rename gff and fasta files by giving list of
        gene ids (filter) and list of ids ref and their corresponding
        alternative ids
        ''')
    parser.add_argument('action', choices=['filter', 'rename'])
    parser.add_argument('-i', '--input', help='FASTA or GFF input file')
    parser.add_argument('-g', '--geneList', help='''
        TSV file containing match between genes ref and alt
        ''')
    parser.add_argument('-o', '--output', help='FASTA or GFF output file')
    args = parser.parse_args()
    return args


def filter_phase_0_gff(finput, fgeneList, foutput):
    gff_df = pd.read_csv(finput, sep='\t', header=None, comment='#')
    genes_df = pd.read_csv(fgeneList, sep='\t', header=None)

    drop_index = []
    for i in range(0, len(gff_df)):
        if "phase_0" in gff_df.iloc[i, 0]:
            gene_id = [g[8:] for g in gff_df.iloc[i, 8].split(';') if "ID" in g]
            genes_res = genes_df[genes_df[0] == gene_id[0]]
            if genes_res.size == 0:
                drop_index.append(i)
    gff_df = gff_df.drop(drop_index)
    gff_df.to_csv(foutput, sep='\t', header=False, index=False)


# Création d'une Séries pandas contenant la liste des correspondances entre les ...
# ... gènes ref et alt
def get_genes_correspondance(fgenelist):
    dict_genes_correspondance = {}
    with open(fgenelist, 'r') as genelist_file:
        count = 0
        for gene in genelist_file:
            split_gene = gene[:-1].split('\t')
            alt, ref = split_gene[:2]
            haplotig = ""
            if split_gene[2]:
                haplotig = split_gene[2]
            if alt in dict_genes_correspondance.keys():
                count += 1
                dict_genes_correspondance[alt].append([ref, haplotig])
            else:
                dict_genes_correspondance[alt] = [[ref, haplotig]]
    return dict_genes_correspondance


# Création de la feature table
def create_db(finput):
    # Cette ligne permet de changer le comportement par défaut de gffutils ...
    # ... ici on demande à ce qu'une liste ne soit pas retournée s'il n'y a qu'un item dedans
    try:
        db = gffutils.create_db(finput, 'gff_db', merge_strategy='create_unique', keep_order=True)
    except  (sqlite3.OperationalError):
        print("Warning: Table features already exists. Supress it if you want to create a new table.\n")
    db = gffutils.FeatureDB('gff_db')
    return db


# Renvoie la liste de tous les enfants (on considère qu'il n'y a pas de gènes dupliqués)
def get_children(gff_db, gene_id):
    # gff_db.children(gene_id) est un générateur, donc il faut le parcourir et mettre tous les ...
    # ... éléments dans une liste
    return [child for child in gff_db.children(gene_id)]


# Fonction permettant de renommer les gènes et mRNA, puis appel de la fonction pour écrire le fichier ...
# ... de sortie
def rename_gff(finput, fgenelist, foutput):
    genes_list_correspondance = get_genes_correspondance(fgenelist)
    gff_db = create_db(finput)
    list_gff_lines = []
    
    for gene in gff_db.features_of_type("gene"):
        alt_name = gene['Name'][0]
        alt_id = gene['ID'][0]
        # Si id_name dans la liste des correspondances --> ajoute attributs id_ref
        if alt_name in genes_list_correspondance.keys():
            count = 1
            correspondances = genes_list_correspondance[alt_name]
            for c in correspondances:
                gene['id_ref{}'.format(count)] = c[0]
                if c[1]:
                    gene['haplotig'] = 'partiel'
                count += 1
        list_gff_lines.append('{}\n'.format(str(gene)))
        children = get_children(gff_db, alt_id)
        for child in children:
            list_gff_lines.append('{}\n'.format(str(child)))

    # écriture du fichier gff avec les attributs id_ref ajoutés
    write_gff(foutput, list_gff_lines)


def write_gff(foutput, list_gff_lines):
    output_file = open(foutput, 'w')
    for line in list_gff_lines:
        output_file.write(line)
    output_file.close()
    

def filter_phase_0_fasta(finput, fgeneList, foutput):
    input_file = open(finput, 'r')
    sequences = fastaparser.Reader(input_file)
    genes_df = pd.read_csv(fgenelist, sep='\t', header=None)

    seq_list = []
    # parcours des séquences et check si le gène vient ...
    # d'un haplotigs phase 0
    for seq in sequences:
        if "phase_0" in seq.id:
            genes_res = genes_df[genes_df[0] == seq.id].values
            # si gène retrouvé dans la liste, on le garde
            if len(genes_res) > 0:
                seq_list.append(seq)
        # et on garde aussi tout ceux qui ne viennent pas ...
        # ... d'un haplotig phase 0
        else:
            seq_list.append(seq)
    input_file.close()

    # écriture du fasta filtré
    write_fasta(seq_list, foutput)


def rename_fasta(finput, fgenelist, foutput):
    # chargement du fichier fasta
    input_file = open(finput, 'r')
    sequences = fastaparser.Reader(input_file)

    # chargement des correspondances ref/alt
    genes = pd.read_csv(fgenelist, sep='\t', header=None)

    # modification de l'id alt en ref si correspondance
    seq_list = []
    for seq in sequences:
        genes_res = genes[genes[0] == seq.id].values
        # si correspondance, renomme l'id de la séquence
        if len(genes_res) > 0: 
            for g in genes_res:
                seq.id = "{0}".format(g[1])
                seq_list.append(seq)
        else:
            seq_list.append(seq)
    input_file.close()

    # ecrite du fichier de sortie contenant les séquences renommées
    write_fasta(seq_list, foutput)


def write_fasta(output, foutput):
    output_file = open(foutput, 'w')
    writer = fastaparser.Writer(output_file)
    for seq in output:
        writer.writefasta(seq)
    output_file.close()
    

if __name__ == '__main__':
    args = command_line()

    #extension = os.path.basename(finput).split('.')[-1]
    #ext_fasta = {'fa', 'fna'}
    #if extension == "gff3":
    #    if choices == 'filter':
    #        filter_phase_0_gff(finput, fgenelist, foutput)
    #    elif choices == 'rename':
    #        rename_gff(finput, fgenelist, foutput)
    #        os.remove("db_genes")
    #elif extension in ext_fasta:
    #    if choices == 'filter':
    #        filter_phase_0_fasta(finput, fgenelist, foutput)
    #    elif choices == 'rename':
    #        rename_fasta(finput, fgenelist, foutput)

    rename_gff(args.input, args.geneList, args.output)