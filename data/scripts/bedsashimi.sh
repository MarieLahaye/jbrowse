#!/bin/bash

directory=$1

if [ "${directory}" != "Riesling" ] && [ "${directory}" != "Gewurztraminer" ]; then
    echo "Argument must be 'Riesling' or 'Gewurztraminer'"
    exit 1
fi

cd /data2/malahaye/data_jbrowse/${directory}/BED_RNAseq/

module load regtools/0.5.2

for i in /data2/malahaye/data_jbrowse/${directory}/BAM_RNAseq/*.bam
do
	file=${i##*/}
	file_name=${file%%.*}
	"Extracting junction from $i..."
	# récupération des jonctions entre les exons (correspondent aux jonctions entre les reads d'une même paire)
	regtools junctions extract -s 2 -t XS $i -o ${file_name}.ori.junctions.bed
	# filtre les jonctions supportées par moins de 3 reads, puis recalcule les bonnes positions start et end
	/var/www/html/jbrowse_dev/data/scripts/junctions_positions.py -i ${file_name}.ori.junctions.bed -o ${file_name}.junctions.bed
	# tri, compression, indexation
	sortBed -i ${file_name}.junctions.bed > ${file_name}.junctions.sorted.bed
	bgzip ${file_name}.junctions.sorted.bed
	tabix -p bed ${file_name}.junctions.sorted.bed.gz
done
