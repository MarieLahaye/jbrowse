#!/usr/bin/env python3
# coding: utf-8
import argparse
from datetime import datetime
import pandas as pd


def command_line():
    parser = argparse.ArgumentParser(description='''
        Filter CDS alignments and generate gff file for alt genes
        ''')
    parser.add_argument('-b', '--blast', required=True, help='BLAST file')
    parser.add_argument('-i', '--intersect', required=True,
                        help='intersect file between reference cds and contigs')
    parser.add_argument('-r', '--ref', required=True,
                        help='GFF file with genes lines only')
    parser.add_argument('-H', '--haplotigs', required=True, help='BED haplotigs file')
    parser.add_argument('-o', '--output', required=True, help='GFF cds alt file')
    args = parser.parse_args()
    return args.blast, args.intersect, args.ref, args.haplotigs, args.output


# Creation d'une série pandas pour lister les correspondances ...
# ... gene --> contig primaire
def series_from_intersect(intersect):
    # dataframe du fichier intersect.tab
    intersect = pd.read_csv(intersect, sep='\t', header=None)
    dict_gc = {}
    # Set des noms de gènes (uniques)
    genes = set(intersect.iloc[:, 3])
    for gene in genes:
        dict_gc[gene] = intersect[intersect[3] == gene][7].values
    # Création d'une série à partir du dict
    series_gc = pd.Series(dict_gc)
    return series_gc


# Filtre alignements des cds sur des mauvais contigs
def wrong_alignment(blast_df, series_gc):
    n = len(blast_df)
    # Index des indices des lignes à supprimer
    drop_index = []
    for i in range(0, n):
        ref = blast_df.iloc[i, 1]
        alt = blast_df.iloc[i, 0]
        # Series_gc[ref] - recherche le cds ref en utilisant l'index de la série
        # Check si l'alignement est sur un gène situé sur le bon contig
        if not alt[:7] in [c[:7] for c in series_gc[ref]]:
            drop_index.append(i)
    return blast_df.drop(drop_index)


def series_from_haplotigs(fhaplotigs):
    haplotigs_df = pd.read_csv(fhaplotigs, sep='\t', header=None)
    dict_haplotigs = {}

    # Récupère la liste des haplotigs
    haplotigs = set(haplotigs_df.iloc[:, 3])

    for h in haplotigs:
        # h[1] = start et h[2] = end
        search = haplotigs_df[haplotigs_df[3] == h].values
        dict_haplotigs[h] = []
        # Au cas où il y aurait des haplotigs coupés en plusieurs morceaux
        for s in search:
            dict_haplotigs[h].append([s[3], s[1], s[2]])
    # Création d'une série à partir du dict
    series_h = pd.Series(dict_haplotigs)
    return series_h


# Chargement du fichier gff en ne gardant que les lignes ...
# ... correspondants aux gènes
def load_gff_df(fref):
    gff_df = pd.read_csv(fref, sep='\t', header=None, comment='#')
    return gff_df[gff_df[2] == "gene"]


def series_from_gff(fref):
    gff_df = load_gff_df(fref)
    dict_ref = {}

    # Récupération de la liste des gènes ref
    list_genes = [g[5:] for gene in gff_df.iloc[:, 8]
                  for g in gene.split(';')
                  if "Name" in g]
    for gene in list_genes:
        dict_ref[gene] = []
        search = gff_df[gff_df[8].str.contains(gene)].values
        for s in search:
            name = [j[5:] for j in s[8].split(';') if "Name" in j][0]
            dict_ref[gene].append([s[3], s[4], s[0], name])
    series_gff = pd.Series(dict_ref)
    return series_gff


# Filtre les alignements en fonction des coordonnées, s'il est situé ...
# ... entre les bornes de l'haplotig correspondant
# On a déjà gardé les alignements situés sur les bons contigs primaires ...
# ... donc pas besoin de vérifier qu'ils sont sur le même chromosome que ...
# ... l'haplotig
def filter_haplotigs(blast_df, series_h, series_gff):
    n = len(blast_df)
    blast_df = blast_df.reset_index(drop=True)

    # Index des indices des lignes à supprimer
    drop_index = []
    for i in range(0, n):
        alt = blast_df.iloc[i, 0]
        ref = blast_df.iloc[i, 1]
        haplotig = alt[:-8]
        search = series_gff[ref]

        # On le passe à True si au moins le start ou le end ...
        # ... est compris dans les coordonnées de l'haplotig
        find = False
        if haplotig in series_h.index:
            for s in search:
                start = s[0]
                end = s[1]
                haplo_res = series_h[haplotig]
                for h in haplo_res:
                    if start in range(h[1], h[2]) or end in range(h[1], h[2]):
                        find = True
        else:
            drop_index.append(i)

        if not find:
            drop_index.append(i)

    return blast_df.drop(drop_index)


def multiple_alignments(blast_df):
    drop_index = []
    max_alt = ""
    n = len(blast_df)

    # Penser à reset les index si modif la dataframe
    blast_df = blast_df.reset_index(drop=True)
    for i in range(0, n):
        alt = blast_df.iloc[i, 0]
        ref = blast_df.iloc[i, 1]
        # Test si change de cds alt
        if alt == max_alt:
            score = float(blast_df.iloc[i, 2])*float(blast_df.iloc[i, 3])
            if score < max_score:
                drop_index.append(i)
            elif score > max_score:
                max_score = score
                if index_egal:
                    drop_index.append(index_egal)
                drop_index.append(index_max)
                index_max = i
            elif score == max_score:
                index_egal = i
        else:
            index_max = i
            index_egal = None
            max_alt = alt
            max_score = float(blast_df.iloc[i, 2])*float(blast_df.iloc[i, 3])
    return blast_df.drop(drop_index)


# Filtre du fichier blast pour ne garder que les bons alignements
def filter(blast, fintersect, fhaplotigs):
    blast_df = pd.read_csv(blast, sep='\t', header=None)

    # Création d'une série contenant les cds alt et leurs contigs ...
    # ... primaires associés
    startTime = datetime.now()
    print('Creation serie cds ref/primary contig...')
    series_gc = series_from_intersect(fintersect)
    print('Timer 1: ', datetime.now() - startTime, '\n')

    n1 = len(blast_df)
    print('Number of alignments: ', n1, '\n')

    # Filtre des mauvais alignements - cds alt non alignés sur contig ...
    # ... primaire qui ne lui correspond pas
    startTime = datetime.now()
    print('Alignments filtration...')
    blast_df = wrong_alignment(blast_df, series_gc)
    print('Timer 2: ', datetime.now() - startTime, '\n')

    n2 = len(blast_df)
    print('Nbr of alignments after filtration: ', n2, '\n')

    # Création d'une série comprenant start et end
    print('Serie from haplotigs...')
    startTime = datetime.now()
    series_h = series_from_haplotigs(fhaplotigs)
    print('Timer 3: ', datetime.now() - startTime, '\n')

    # Création d'une série des genes ref
    print('Series from gff...')
    startTime = datetime.now()
    series_gff = series_from_gff(fref)
    print('Timer 4: ', datetime.now() - startTime, '\n')

    # Filtre alignements de cds sur les mauvais haplotigs
    print('Filter by haplotigs...')
    startTime = datetime.now()
    blast_df = filter_haplotigs(blast_df, series_h, series_gff)
    print('Timer 5: ', datetime.now() - startTime, '\n')

    n3 = len(blast_df)
    print('Nbr of alignments after filtration: ', n3, '\n')

    # Filtre alignements multiples pour un cds alt sur le même contig primaire
    print('Multiple alignments filtration...')
    startTime = datetime.now()
    blast_df = multiple_alignments(blast_df)
    print('Timer 6: ', datetime.now() - startTime, '\n')

    n4 = len(blast_df)
    print('Nbr of alignments after filtration: ', n4, '\n')
    return blast_df, series_h, series_gff


# A garder, certaines étapes peuvent être utiles pour filtrer le gff alt
def bed_dataframe(blast_df, series_gff, series_h):
    blast_df = blast_df.reset_index(drop=True)
    genes = []
    cds_list = []
    dict_ref = {}

    for gene in series_gff:
        for g in gene:
            # Récupère le nom du gène dans la dernière colonne "Name=nomgène"
            ref = g[3]
            start = g[0]
            end = g[1]
            chrom = g[2]

            # Recherche des alignements sur le gène observé pour récupérer ...
            # ... les id_alt
            search_list = blast_df[blast_df[1] == ref].values

            # Si des match sont trouvés
            if search_list.size > 0:
                score = 0
                compteur = 0
                for s in search_list:
                    note = None
                    line = False
                    cds = None
                    alt = s[0]

                    for h in series_h[alt[:-8]]:
                        hstart = h[1]
                        hend = h[2]

                        # Test si start et end du gène sont bien compris sur ...
                        # ... un haplotig représenté sur le jbrowse
                        if start in range(hstart, hend):
                            if end in range(hstart, hend):
                                line = True
                            else:
                                line = True
                                note = "haplotig=partiel"
                        else:
                            if end in range(hstart, hend):
                                line = True
                                note = "haplotig=partiel"
                    # Pour chaque cds si on a un bon match on l'ajoute dans la ...
                    # ... liste des gènes alt ref
                    if line:
                        compteur += 1
                        score += s[2]

                        # Dict pour compter le nb d'occurrence du gène ref dans ...
                        # ... la liste des gènes
                        if ref in dict_ref.keys():
                            dict_ref[ref] += 1
                        else:
                            dict_ref[ref] = 1

                        if note:
                            cds = [alt, "{0}.{1}".format(ref, dict_ref[ref]), note]
                        else:

                            cds = [alt, "{0}.{1}".format(ref, dict_ref[ref])]

                    if cds:
                        cds_list.append(cds)

                if compteur > 0:
                    score = score/compteur
                    gene = [chrom, start, end, ref, score]
                    genes.append(gene)

    alt_df = pd.DataFrame(cds_list)
    alt_df.to_csv("genes_list.tsv", sep='\t', header=False, index=False)
    output_df = pd.DataFrame(genes)
    return output_df


if __name__ == '__main__':
    fblast, fintersect, fref, fhaplotigs, foutput = command_line()
    blast_df, series_h, series_gff = filter(fblast, fintersect, fhaplotigs)

    print("BED writing...")
    startTime = datetime.now()
    output_df = bed_dataframe(blast_df, series_gff, series_h)
    print("Timer 7: ", datetime.now() - startTime, '\n')

    print("Number of genes: ", len(output_df))
    output_df.to_csv(foutput, sep='\t', header=False, index=False)
