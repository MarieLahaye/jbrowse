#!/usr/bin/bash

# check l'argument de la ligne de commande
directory=$1

if [ "${directory}" != "Riesling" ] && [ "${directory}" != "Gewurztraminer" ]; then
    echo "Argument must be 'Riesling' or 'Gewurztraminer'"
    exit 1
fi

# récupère dans une variable le sygle RI ou GW en fonction du répertoire 
if [ "${directory}" == "Riesling" ]; then
	sygle="RI"
elif [ "${directory}" == "Gewurztraminer" ]; then
	sygle="GW"
fi

cd /data2/malahaye/data_jbrowse/data/${directory}/WIG_RNAseq/

for i in /data2/malahaye/data_jbrowse/data/${directory}/BAM_RNAseq/*.bam
do
	file=${i##*/}
	file_name=${file%%.*}
	# normalisation avec le RPKM et option pour indiquer 1 bin --> 1 base
	sbatch -c 4 -w node003 bamCoverage --bam $i --normalizeUsing RPKM --binSize 1 --outFileName ${file_name}.normalized.bw
done
